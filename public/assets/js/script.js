const canvas = document.getElementById("the_canvas");
const ctx = canvas.getContext("2d");

let gameVisibility = document.getElementById("game");
let docVisibility = document.getElementById("non-game");
let savedData = document.getElementById("saved-data");

let adiv = document.getElementById('clock');
let scoreDiv = document.getElementById('score');

let playerImage = new Image();
playerImage.src = "assets/img/DancerSpritesheet.png";

let player2Image = new Image();
player2Image.src = "assets/img/Gunbreaker.png";

let chackramImage = new Image();
chackramImage.src = "assets/img/Chackram.png";
let chackramSpeedX = 0;
let chackramSpeedY = 0;

let healthTaken = true;

function GameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
}

let frameNoX = 0;
let frameNoY = 0;
const scale = 0.7;
const width = 97;
const height = 188;
const scaledWidth = scale * width;
const scaledHeight = scale * height;
let totalScore = 0;

function AnimatedGameObject(spritesheet, x, y, width, height) {
    this.spritesheet = spritesheet;
    this.frameX = width * frameNoX;
    this.frameY = height * frameNoY;
    this.Width = width;
    this.Height = height;
    this.canvasX = x;
    this.canvasY = y;
    this.scaledWidth = width * scale;
    this.scaledHeight = height * scale;
}

let player = new AnimatedGameObject(playerImage, 200, 300, 105, 120);
let player2 = new GameObject(player2Image, 1000, 300, 106, 125)
let chackram = new GameObject(chackramImage, 2000, 0, 119, 119)

function GamerInput(input) {
    this.action = input;
}
let gamerInput = new GamerInput("None");
let gamer2Input = new GamerInput("None");
let direction = new GamerInput("Left");

function input(event) 
{
    if (event.type === "keydown") 
    {
        switch (event.keyCode) 
        {
            //change size of player 2
            case 32: // Space
            gamer2Input = new GamerInput("Space");
            break;

            // Keys for player 1
            case 37: // Left Arrow
                gamerInput = new GamerInput("Left");
                direction = new GamerInput("Left");
                break;
            case 38: // Up Arrow
                gamerInput = new GamerInput("Up");
                direction = new GamerInput("Up");
                break;
            case 39: // Right Arrow
                gamerInput = new GamerInput("Right");
                direction = new GamerInput("Right");
                break;
            case 40: // Down Arrow
                gamerInput = new GamerInput("Down");
                direction = new GamerInput("Down");
                break;
            case 96: // Insert 
                gamerInput = new GamerInput("Insert");
                break;


            // Keys for player 2
            case 87: // W
                gamer2Input = new GamerInput("W");
                break;
            case 65: // A
                gamer2Input = new GamerInput("A");
                break;
            case 83: // S
                gamer2Input = new GamerInput("S");
                break;
            case 68: // D
                gamer2Input = new GamerInput("D");
                break;


            case 82: // R
            gamerInput = new GamerInput("R");
            break;

            default:
                gamerInput = new GamerInput("None"); //No Input 
                gamer2Input = new GamerInput("None"); //No Input 
                console.log
        }
    }
    if (event.type === 'mousedown')
    {
        console.log("*click*");
        if (event.target.className === "button yellow up")
        {
            gamer2Input = new GamerInput("W");
        } 
        if (event.target.className === "button blue left")
        {
            gamer2Input = new GamerInput("A");
        } 
        if (event.target.className === "button green down")
        {
            gamer2Input = new GamerInput("S");
        } 
        if (event.target.className === "button red right")
        {
            gamer2Input = new GamerInput("D");
        } 
    } 
    else if (event.type !== 'mousedown' && event.type !== 'keydown')
    {
        gamerInput = new GamerInput("None");
        gamer2Input = new GamerInput("None");
    }
}

let speed = 10;
let offScreen = true;
let small = false;
let timer = 100;

const walkLoop = [2, 0, 2, 4];
let currentLoopIndex = 0;
let frameCount = 0;
let directionNumber = 0;
let playerScale = 1;
let frameSpeed = 8;

loadGame()

if (localStorage.getItem("p1Health") != undefined)
{
    console.log("Resuming");
    savedData.style.visibility = "visible";
    docVisibility.style.visibility = "hidden";
}
if (localStorage.getItem("p1Health") == undefined)
{
    console.log("First load");
    let player1Health = 100;
    let player2Health = 100;
}

if (gameVisibility.style.visibility === "visible")
{
    savedData.style.visibility = "hidden";
}

function update()
{
    clock()
    timer++;
    score()



    if (!offScreen)
    {
        chackram.x += chackramSpeedX;
        chackram.y += chackramSpeedY;
    }

    // move player
    if (gamerInput.action === "Up") {
        console.log("Move Up");
        player.canvasY -= speed;
        directionNumber = 1;
        frameCount++;
    } else if (gamerInput.action === "Down") {
        console.log("Move Down");
        player.canvasY += speed;
        directionNumber = 0;
        frameCount++;
    } else if (gamerInput.action === "Left") {
        console.log("Move Left");
        player.canvasX -= speed;
        directionNumber = 2;
        playerScale = 1;
        frameCount++;
    } else if (gamerInput.action === "Right") {
        console.log("Move Right");
        player.canvasX += speed;
        directionNumber = 3;
        playerScale = -1
        frameCount++;
    }
    else if (gamerInput.action != "Up" &&
             gamerInput.action != "Down" &&
             gamerInput.action != "Right" &&
             gamerInput.action != "Left") 
    {
        player.frameX = 2 + (directionNumber * 6);
        frameCount = frameSpeed - 1;
    }

    if (gamerInput.action === "R")
    {
        player1Health = 100;
        player2Health = 100;
        totalScore = 0;
        localStorage.setItem("p1Health", player1Health);
        localStorage.setItem("p2Health", player2Health);
        localStorage.setItem("score", totalScore);
    }

    // chackram spawning
    if (gamerInput.action === "Insert" && offScreen === true){
        spawnChackram()
        offScreen = false;
        healthTaken = false;
    }
    if (chackram.x > 1760 || chackram.x < 0 - chackram.width || chackram.y > 805 || chackram.y < 0 - chackram.height)
    {
        offScreen = true;
    }

    if (offScreen === true && healthTaken === false)
    {
        player1Health -= 10;
        healthTaken = true;
        localStorage.setItem("p1Health", player1Health);
        console.log(localStorage.getItem("p1Health"));
    }

    // collision between chackram and player2
    if (chackram.x > player2.x - (player2.width / 2) &&
        chackram.x < player2.x + (player2.width / 2) &&
        chackram.y > player2.y - (player2.width / 2)&&
        chackram.y < player2.y + (player2.height / 2))
    {
        offScreen = true;
        healthTaken = true;
        chackram.x = 2000;
        player2Health -= 10;
        totalScore = parseInt(totalScore) + 1;
        console.log(totalScore);
        localStorage.setItem("score", totalScore);
        localStorage.setItem("p2Health", player2Health);
    }
    

    // move enemy
    if (gamer2Input.action === "W") {
        console.log("Move Up");
        player2.y -= speed;
    } else if (gamer2Input.action === "S") {
        console.log("Move Down");
        player2.y += speed;
    } else if (gamer2Input.action === "A") {
        console.log("Move Left");
        player2.x -= speed;
    } else if (gamer2Input.action === "D") {
        console.log("Move Right");
        player2.x += speed;

    }
    else if (gamer2Input.action === "Space" && timer > 60) {
        if (small)
        {
            player2.width = 106;
            player2.height = 125;
            small = false;
            timer = 0;
        }
        else 
        {
            player2.width = 53;
            player2.height = 62;
            small = true;
            timer = 0;
        }
    }
    if (timer > 60 && small)
    {
        player2.width = 106;
        player2.height = 125;
        small = false;
        timer = 0;
    }

    // boundary check for player
    if (player.canvasY <= -5)
    {
        player.canvasY += speed;
    }
    if (player.canvasY >= 805 - player.scaledHeight * 1.5)
    {
        player.canvasY -= speed;
    }
    if (player.canvasX <= -6)
    {
        player.canvasX += speed;
    }
    if (player.canvasX >= 1760 - player.scaledWidth)
    {
        player.canvasX -= speed;
    }

    // boundary check for enemy
    if (player2.y <= -5)
    {
        player2.y += speed;
    }
    if (player2.y >= 810 - player2.height)
    {
        player2.y -= speed;
    }
    if (player2.x <= -6)
    {
        player2.x += speed;
    }
    if (player2.x >= 1760 - player2.width)
    {
        player2.x -= speed;
    }

    if (frameCount >= frameSpeed)
    {
        console.log (walkLoop[currentLoopIndex]);
        player.frameX = walkLoop[currentLoopIndex] + (directionNumber * 6)
        player.frameY = 0;
        currentLoopIndex++;
        if (currentLoopIndex >= walkLoop.length) {
          currentLoopIndex = 0;
          console.log("changing direction to dir: " + direction.action);
        }
        frameCount = 0;
    }
    
}

let dynamic = nipplejs.create({
    zone: document.getElementById('joystick_zone'),
    color: 'red',
});

dynamic.on('added', function (event, joyStick) 
{
    console.log("Created joystick");

    joyStick.on('dir:up', function(event, data)
    {
        console.log("UP");
        gamerInput = new GamerInput("Up");
    });
    joyStick.on('dir:down', function(event, data)
    {
        console.log("DOWN");
        gamerInput = new GamerInput("Down");
    });
    joyStick.on('dir:left', function(event, data)
    {
        console.log("LEFT");
        gamerInput = new GamerInput("Left");
    });
    joyStick.on('dir:right', function(event, data)
    {
        console.log("RIGHT");
        gamerInput = new GamerInput("Right");
    });
    joyStick.on('end', function(event, data)
    {
        console.log("none");
        gamerInput = new GamerInput("None");
    });
});

function spawnChackram()
{
    chackramSpeedX = 0;
    chackramSpeedY = 0;
    chackram.x = player.canvasX;
    chackram.y = player.canvasY;
    if (direction.action === "Up")
    {
        chackramSpeedY -= 10;
    }
    else if (direction.action === "Down")
    {
        chackramSpeedY += 10;
    }
    else if (direction.action === "Left")
    {
        chackramSpeedX -= 10;
    }
    else if (direction.action === "Right")
    {
        chackramSpeedX += 10;
    }
}

// Draw a HealthBar on Canvas, can be used to indicate players health
  function drawPlayerHealthbar() {
    let width = 100;
    let height = 20;
    let max = 100;
  
    // Draw the background
    ctx.fillStyle = "#000000";
    ctx.fillRect(player.canvasX - 20, player.canvasY - 30, width, height);
  
    // Draw the fill
    ctx.fillStyle = "#00FF00";
    let fillVal = Math.min(Math.max(player1Health / max, 0), 1);
    ctx.fillRect(player.canvasX - 20, player.canvasY - 30, fillVal * width, height);
  }

  function drawEnemyHealthbar() {
    let width = 100;
    let height = 20;
    let max = 100;
  
    // Draw the background
    ctx.fillStyle = "#000000";
    ctx.fillRect(player2.x, player2.y - 30, width, height);
  
    // Draw the fill
    ctx.fillStyle = "#00FF00";
    let fillVal = Math.min(Math.max(player2Health / max, 0), 1);
    ctx.fillRect(player2.x, player2.y - 30, fillVal * width, height);
  }
  
  
function validateForm(){
    console.log("FORMVALIDATED!");
    if (localStorage.getItem("p1Health") == undefined)
    {
        let name = document.forms["FirstRunForm"]["userName"].value;
        if(name == "")
        {
            alert("put letters in the box plz -_-");
            return false;
        }
        else{
            alert("Hey, " + name);
            loadGame();
        }
    }

    let answer = document.querySelector('#no');
    if(answer.checked == true)
    {
        docVisibility.style.visibility = "shown";
        savedData.style.visibility = "hidden";

        player1Health = 100;
        player2Health = 100;
        totalScore = 0;
        localStorage.setItem("p1Health", player1Health);
        localStorage.setItem("p2Health", player2Health);
        localStorage.setItem("score", totalScore);
    }
}

function validateSaveCheck()
{
    
}


function clock()
{
    let date = new Date();

    adiv.innerHTML = (+date.getHours()
    +":"+date.getMinutes()
    +":"+date.getSeconds()
    +"<br>" 
    + date.getDate()
    +"/"+(date.getMonth()+1)
    +"/"+date.getFullYear());
}

function score()
{
    scoreDiv.innerHTML = ("Score: " + totalScore);
}

function loop()
{
    update();
    draw();
    window.requestAnimationFrame(loop)
}

function draw() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    if (player2Health > 0)
    {
        ctx.drawImage(player2.spritesheet, 
            player2.x,
            player2.y,
            player2.width,
            player2.height);

        drawEnemyHealthbar();
    }
    if (player1Health > 0)
    {
        ctx.drawImage(player.spritesheet,
            player.frameX * width, player.frameY * height, width, height,
            player.canvasX, player.canvasY, scaledWidth, scaledHeight);

        drawPlayerHealthbar();
    }
    ctx.drawImage(chackram.spritesheet, 
                  chackram.x,
                  chackram.y,
                  chackram.width,
                  chackram.height);
}

function loadGame(){

    let s = document.forms["FirstRunForm"]["userName"].value;
    player1Health = localStorage.getItem("p1Health");
    player2Health = localStorage.getItem("p2Health");
    totalScore = localStorage.getItem("score");
    localStorage.setItem("name", s);

    document.getElementById("main-header").innerHTML = localStorage.getItem("name");
    let queryString = document.location.search;
    let name = queryString.split("=")[1];
    if (name != undefined){
        docVisibility.style.visibility = "hidden";
        savedData.style.visibility = "hidden";
        gameVisibility.style.visibility = "visible";
    }

}

window.requestAnimationFrame(loop);

window.addEventListener('keydown', input);
window.addEventListener('keyup', input);
window.addEventListener('mousedown', input);
window.addEventListener('mouseup', input);
window.addEventListener('click', input);